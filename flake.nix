{
  description = "Point of Sale App";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    parts.url = "github:hercules-ci/flake-parts";
    parts.inputs.nixpkgs-lib.follows = "nixpkgs";
    devenv.url = "github:cachix/devenv";
    nci.url = "github:yusdacra/nix-cargo-integration";
    nci.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = inputs@{ parts, devenv, nci,... }:
    parts.lib.mkFlake { inherit inputs; } {
      imports = [ devenv.flakeModule nci.flakeModule ];
      systems = [
        "x86_64-linux"
        # "i686-linux"
        # "x86_64-darwin"
        # "aarch64-linux"
        # "aarch64-darwin"
      ];

      perSystem = { config, self', inputs', pkgs, system, ... }:
        let

          inherit (pkgs) lib;
          # our specific toolchain there.
          # craneLib = (crane.mkLib pkgs).overrideToolchain rustTarget;
          # Only keeps markdown files
          # protoFilter = path: _type: builtins.match ".*proto$" path != null;
          # sqlxFilter = path: _type: builtins.match ".*json$" path != null;
          # sqlFilter = path: _type: builtins.match ".*sql$" path != null;
          # cssFilter = path: _type: builtins.match ".*css$" path != null;
          # ttfFilter = path: _type: builtins.match ".*ttf$" path != null;
          # woff2Filter = path: _type: builtins.match ".*woff2$" path != null;
          # webpFilter = path: _type: builtins.match ".*webp$" path != null;
          # jpegFilter = path: _type: builtins.match ".*jpeg$" path != null;
          # pngFilter = path: _type: builtins.match ".*png$" path != null;
          # icoFilter = path: _type: builtins.match ".*ico$" path != null;
          # protoOrCargo = path: type:
          #   (protoFilter path type) || (craneLib.filterCargoSources path type) || (sqlxFilter path type) || (sqlFilter path type) || (cssFilter path type) || (woff2Filter path type) || (ttfFilter path type) || (webpFilter path type) || (icoFilter path type) || (jpegFilter path type) || (pngFilter path type);
          # # other attributes omitted
          #
          # # Include more types of files in our bundle
          # src = lib.cleanSourceWith {
          #   src = ./.; # The original, unfiltered source
          #   filter = protoOrCargo;
          # };
          #    src = craneLib.cleanCargoSource ./.;

          # Common arguments can be set here
          commonArgs = {
            # inherit src;
          buildInputs = [
            # Add additional build inputs here
            # pkgs.leptos
            pkgs.cargo-leptos
            pkgs.pkg-config
            pkgs.openssl
            pkgs.protobuf
            pkgs.binaryen
            pkgs.cargo-generate
          ] ++ lib.optionals pkgs.stdenv.isDarwin [
            # Additional darwin specific inputs can be set here
            pkgs.libiconv
          ];
        };
          #
          #
          # # Build *just* the cargo dependencies, so we can reuse
          # # all of that work (e.g. via cachix) when running in CI
          # cargoArtifacts = craneLib.buildDepsOnly (commonArgs // {
          #   cargoExtraArgs = " --target x86_64-unknown-linux-gnu";
          #   doCheck = false;
          #    # Needed to enable build-std inside Crane
          #   cargoVendorDir = craneLib.vendorMultipleCargoDeps {
          #     inherit (craneLib.findCargoFiles src) cargoConfigs;
          #     cargoLockList = [
          #     
          #     # cargoLock.lockFile = ./Cargo.lock;
          #
          #       # Unfortunately this approach requires IFD (import-from-derivation)
          #       # otherwise Nix will refuse to read the Cargo.lock from our toolchain
          #       # (unless we build with `--impure`).
          #       #
          #       # Another way around this is to manually copy the rustlib `Cargo.lock`
          #       # to the repo and import it with `./path/to/rustlib/Cargo.lock` which
          #       # will avoid IFD entirely but will require manually keeping the file
          #       # up to date!
          #       "${rustTarget.passthru.availableComponents.rust-src}/lib/rustlib/src/rust/Cargo.lock"
          #     ];
          #   };
          # });
          #
          # # Build the actual crate itself, reusing the dependency
          # # artifacts from above.
          # benwis_leptos = craneLib.buildPackage (commonArgs // {
          #   pname = "pos";
          #
          # # Needed to enable build-std inside Crane
          # cargoVendorDir = craneLib.vendorMultipleCargoDeps {
          #   inherit (craneLib.findCargoFiles src) cargoConfigs;
          #     # cargoLock.lockFile = ./Cargo.lock;
          #   cargoLockList = [
          #   #   ./Cargo.lock
          #   #
          #   #   # Unfortunately this approach requires IFD (import-from-derivation)
          #   #   # otherwise Nix will refuse to read the Cargo.lock from our toolchain
          #   #   # (unless we build with `--impure`).
          #   #   #
          #   #   # Another way around this is to manually copy the rustlib `Cargo.lock`
          #   #   # to the repo and import it with `./path/to/rustlib/Cargo.lock` which
          #   #   # will avoid IFD entirely but will require manually keeping the file
          #   #   # up to date!
          #     "${rustTarget.passthru.availableComponents.rust-src}/lib/rustlib/src/rust/Cargo.lock"
          #   ];
          # };
          #
          #   buildPhaseCargoCommand = "cargo leptos build --release -vvv";
          #   installPhaseCommand = ''
          #   mkdir -p $out/bin
          #   cp target/server/x86_64-unknown-linux-gnu/release/benwis_leptos $out/bin/
          #   cp -r target/site $out/bin/
          #   '';
          #   # Prevent cargo test and nextest from duplicating tests
          #   doCheck = false;
          #   #cargoExtraArgs: "-Z build-std --target "
          #   inherit cargoArtifacts;
          #   # ALL CAPITAL derivations will get forwarded to mkDerivation and will set the env var during build
          #   SQLX_OFFLINE = "true";
          #   LEPTOS_BIN_TARGET_TRIPLE = "x86_64-unknown-linux-gnu"; # Adding this allows -Zbuild-std to work and shave 100kb off the WASM
          #   LEPTOS_BIN_PROFILE_RELEASE = "release";
          #   LEPTOS_LIB_PROFILE_RELEASE ="release-wasm-size";
          #   APP_ENVIRONMENT = "production";
          # });
          rustTarget = pkgs.rust-bin.selectLatestNightlyWith (toolchain: toolchain.default.override {
            extensions = [ "rust-src" "rust-analyzer" ];
            targets = [ "wasm32-unknown-unknown" ];
          });
          # TODO: change this to your crate's name
          crateName = "pos";
          # shorthand for accessing this crate's outputs
          # you can access crate outputs under `config.nci.outputs.<crate name>` (see documentation)
          crateOutputs = config.nci.outputs.${crateName};

        in {

          _module.args.pkgs = import inputs.nixpkgs {
            inherit system;
            overlays = [
              (import inputs.rust-overlay)
              ];
          };

          # Per-system attributes can be defined here. The self' and inputs'
          # module parameters provide easy access to attributes of the same
          # system.
          # Equivalent to  inputs'.nixpkgs.legacyPackages.hello;
          # packages.default = pkgs.hello;
          devenv.shells.default = {
            # name = "pos";
            # https://devenv.sh/reference/options/
            packages = with pkgs; [ 
            # config.packages.default 
              rustTarget
              # openssl
              # mysql80
              # dive
              # sqlx-cli
              wasm-pack
              pkg-config
              binaryen
              # nodejs
              # hey
              # drill
              # nodePackages.tailwindcss
              cargo-leptos
              protobuf
              # skopeo
              # flyctl
            ];
            languages.rust.enable = true;
            languages.nix.enable = true;
            # enterShell = "";
          };


          nci = {
            projects.${crateName} = {
              relPath = "";
              # profiles = {
              #   dev.runTests = false;
              #   release.runTests = true;
              # };
            };
            crates.${crateName} = {
            export = true;

             depsOverrides.set-target = {
             # set cargo target to WASM so it compiles correctly
             CARGO_BUILD_TARGET = "wasm32-unknown-unknown";
             };
              profiles = {
                # dev.runTests = false;
                release.runTests = false;
              };
            overrides = {


                # add-env = {TEST_ENV = 1;};
                add-inputs.overrideAttrs = old: {
                  nativeBuildInputs= (old.nativeBuildInputs or []) ++ commonArgs.buildInputs;

            buildPhase = "cargo leptos build --release -vvv";
            installPhase = ''
            mkdir -p $out/bin
            cp target/server/x86_64-unknown-linux-gnu/release/${crateName} $out/bin/
            cp -r target/site $out/bin/
            '';
                };
              };
            # toolchains.build = rustTarget;
          };};
          packages.default = crateOutputs.packages.release;
          # apps.default = crateOutputs.packages.default;
          # packages.default = benwis_leptos;

          # apps.default = flake-utils.lib.mkApp {
          #   drv = benwis_leptos;
          # };
        };
      flake = {
        # The usual flake attributes can be defined here, including system-
        # agnostic ones like nixosModule and system-enumerating ones, although
        # those are more easily expressed in perSystem.

      };
    };
}
